plugins {
    id("nl.rug.digitallab.gradle.plugin.kotlin.library")
}

fun DependencyHandler.hiddenImplementation(dependencyNotation: Any) {
    compileOnly(dependencyNotation)
    testImplementation(dependencyNotation)
}

dependencies {
    val commonsIoVersion: String by project
    val kotlinxCoroutinesVersion: String by project
    val quarkusVersion: String by project
    val sprinklerUtilsVersion: String by project

    api("commons-io:commons-io:$commonsIoVersion")
    api("com.black-kamelia.sprinkler:utils:$sprinklerUtilsVersion") // closeableScope

    // We include the Quarkus BOM as a hidden implementation to allow our hidden
    // dependencies to be of the same version of our current primary Quarkus version
    hiddenImplementation(platform("io.quarkus.platform:quarkus-bom:$quarkusVersion"))

    // We include these dependencies as "compile only", and then as implementation
    // in the test configuration. This way, we don't pollute the classpaths of a
    // user with the entirety of Quarkus when they don't use it and don't need it.
    // At the same time, we do (sneakily) include the actual implementations in the
    // library, so that when these dependencies are available, the integrations
    // are available and automatically picked up by the framework through DI.
    hiddenImplementation("jakarta.annotation:jakarta.annotation-api")
    hiddenImplementation("io.smallrye.config:smallrye-config")

    // We need to explicitly include ASM again, since the Quarkus BOM has this as an
    // excluded transient dependency for Smallrye
    hiddenImplementation("org.ow2.asm:asm")

    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
}

tasks.test {
    useJUnitPlatform()
}
