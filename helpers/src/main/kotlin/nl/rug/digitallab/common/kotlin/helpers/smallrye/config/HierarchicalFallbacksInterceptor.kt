package nl.rug.digitallab.common.kotlin.helpers.smallrye.config

/**
 * This in-between abstract class helps to implement a
 * [FallbacksInterceptor] where a specific handling logic needs
 * to be applied to each path component of the property name
 * separately. This implementation will then try all combinations
 * of modified and unmodified path components to resolve a
 * configuration value.
 *
 * For example; when [getFallbacksForPart], given "a", returns
 * "a1", "a2", "a3"; then the input path "a.b" would result in
 * the following list of names being tried:
 * a1.b1, a1.b2, a1.b3, a2.b1, a2.b2, a2.b3, a3.b1, a3.b2, a3.b3
 */
abstract class HierarchicalFallbacksInterceptor : FallbacksInterceptor() {
    /**
     * Function to transform a single path components into a list
     * of fallback values to try.
     *
     * @param name The name component/part to transform
     */
    internal abstract fun getFallbacksForPart(name: String) : Set<String>

    override fun getFallbacks(name: String): Set<String> =
        // We split the name into its parts, then for each part
        // we find the list of fallbacks, and we form all possible
        // combinations. We do this using a fold with a nested flatmap,
        // which essentially implements a cartesian product.
        name
            .split(".")
            .fold(listOf(emptyList<String>())) { names, part ->
                getFallbacksForPart(part).flatMap {
                    names.map { name -> name + it }
                }
            }.map { it.joinToString(".") }
            .toSet()
}
