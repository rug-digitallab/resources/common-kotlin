package nl.rug.digitallab.common.kotlin.helpers.noarg

@Target(AnnotationTarget.CLASS, AnnotationTarget.ANNOTATION_CLASS)
annotation class NoArgConstructor
