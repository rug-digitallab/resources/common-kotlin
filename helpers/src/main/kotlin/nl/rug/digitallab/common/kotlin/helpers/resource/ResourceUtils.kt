package nl.rug.digitallab.common.kotlin.helpers.resource

import com.kamelia.sprinkler.util.closeableScope
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.net.URL
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Load a resource from the classpath (src/ * /resources).
 *
 * @param path The relative path to the resource, from inside the resources folder.
 *
 * @return The URL of the resource.
 */
fun getResource(path: String): URL =
    Thread.currentThread().contextClassLoader.getResource(path)
        ?: throw IllegalArgumentException("Resource $path not found")

/**
 * Load a resource from the classpath (src/ * /resources).
 *
 * @param path The relative path to the resource, from inside the resources folder.
 *
 * @return The URL of the resource.
 */
fun getResource(path: Path): URL = getResource(path.toString())

/**
 * Load a resource from the classpath (src/ * /resources) as an input stream.
 *
 * @param path The relative path to the resource, from inside the resources folder.
 *
 * @return The input stream of the resource.
 */
fun getResourceAsStream(path: String): InputStream =
    Thread.currentThread().contextClassLoader.getResourceAsStream(path)
        ?: throw IllegalArgumentException("Resource $path not found")

/**
 * Load a resource from the classpath (src/ * /resources) as an input stream.
 *
 * @param path The relative path to the resource, from inside the resources folder.
 *
 * @return The input stream of the resource.
 */
fun getResourceAsStream(path: Path): InputStream = getResourceAsStream(path.toString())

/**
 * Lists the resources in the provided path.
 *
 * @param path The path to list the resources from.
 *
 * @return The list of URLs to resources in the provided path.
 */
fun listResources(path: Path): List<URL> {
    val resourceStream = getResourceAsStream(path)
    val resourceReader = resourceStream.bufferedReader()

    return closeableScope(resourceStream, resourceReader) {
        resourceReader.readLines().map { getResource(path.resolve(it)) }
    }
}

/**
 * Lists the resources in the provided path.
 *
 * @param path The path to list the resources from.
 *
 * @return The list of URLs to resources in the provided path.
 */
fun listResources(path: String): List<URL> = listResources(Paths.get(path))

/**
 * Convert a URL to a file. If the URL points to a file in a JAR, this will fail.
 *
 * @return The [File] representation of the URL.
 *
 * @throws FileNotFoundException If the URL does not point to a file on the file system.
 */
fun URL.asFile(): File =
    FileUtils.toFile(this) ?: throw FileNotFoundException("The URL ${toExternalForm()} does not point to a file on the file system")

/**
 * Convert a URL to a path. If the URL points to a file in a JAR, this will fail.
 *
 * @return The [Path] representation of the URL.
 *
 * @throws FileNotFoundException If the URL does not point to a file on the file system.
 */
fun URL.asPath(): Path =
    this.asFile().toPath()

/**
 * Copy the contents of a URL to a file. This operation is JAR safe.
 *
 * @param destination The file to copy the contents of the URL to.
 */
fun URL.copyToFile(destination: File): Unit =
    FileUtils.copyURLToFile(this, destination)
