package nl.rug.digitallab.common.kotlin.helpers.delegate

import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1

/**
 * [ConversionDelegate] is a read-only delegate implementation that allows
 * a user to "wrap" a property of type [Source] and expose a new property
 * of type [Target] with the specified conversion [sourceToTarget].
 *
 * This class is mainly intended as a boilerplate implementation to be
 * used by helper functions for specialised conversions. For one-off
 * conversions, it is likely easier to use a normal custom getter directly.
 *
 * @param ParentType Type of the class in which the properties are defined
 * @param Source Type of the "wrapped" property
 * @param Target Type of the new property
 *
 * @param backingProperty The backing property of type [Source]
 * @param sourceToTarget Conversion from [Source] to [Target]
 */
open class ConversionDelegate<ParentType, Source, Target>(
    open val backingProperty: KProperty1<ParentType, Source>,
    open val sourceToTarget: (Source) -> Target,
) : ReadOnlyProperty<ParentType, Target> {
    override fun getValue(thisRef: ParentType, property: KProperty<*>): Target {
        return backingProperty.get(thisRef).let(sourceToTarget)
    }
}

/**
 * [MutableConversionDelegate] is a read-write delegate implementation that
 * extends [ConversionDelegate] to also allow writing new values. This allows
 * a user to "wrap" a property of type [Source] and expose a new property
 * of type [Target] with the specified conversions.
 *
 * This class is mainly intended as a boilerplate implementation to be
 * used by helper functions for specialised conversions. For one-off
 * conversions, it is likely easier to use a normal custom getter/setter directly.
 *
 * @param ParentType Type of the class in which the properties are defined
 * @param Source Type of the "wrapped" property
 * @param Target Type of the new property
 *
 * @param backingProperty The mutable backing property of type [Source]
 * @param sourceToTarget Conversion from [Source] to [Target]
 * @param targetToSource Conversion from [Target] to [Source]
 */
class MutableConversionDelegate<ParentType, Source, Target>(
    override val backingProperty: KMutableProperty1<ParentType, Source>,
    override val sourceToTarget: (Source) -> Target,
    val targetToSource: (Target) -> Source,
) : ConversionDelegate<ParentType, Source, Target>(backingProperty, sourceToTarget), ReadWriteProperty<ParentType, Target> {
    override fun setValue(thisRef: ParentType, property: KProperty<*>, value: Target) {
        backingProperty.set(thisRef, targetToSource(value))
    }
}
