package nl.rug.digitallab.common.kotlin.helpers.ephemeral

import java.nio.file.Path
import java.nio.file.attribute.FileAttribute
import kotlin.io.path.createTempFile
import kotlin.io.path.deleteIfExists

/**
 * Defines an ephemeral file resource, which is a file that will automatically delete itself once the program exits.
 *
 * To use the resource, use [withEphemeralFile].
 *
 * @param parent The directory in which the ephemeral directory is created in.
 * @param prefix The prefix of the ephemeral file
 * @param suffix The suffix of the ephemeral file
 * @param attributes An optional list of file attributes to set atomically when creating the directory.
 */
open class EphemeralFile(
    parent: Path? = null, prefix: String = "", suffix: String = "", vararg attributes: FileAttribute<*> = arrayOf(),
) : AutoCloseable {
    val path = createTempFile(parent, prefix, suffix, *attributes).also {
        it.toFile().deleteOnExit()
    }

    override fun close() {
        path.deleteIfExists()
    }
}

/**
 * A scope function to create an ephemeral file and run the [block] with the created directory. The primary use
 * is to ensure that the file is deleted once there is no reference to it. NOTE: these semantics are only ensured
 * as long as the block doesn't copy its parameter to a variable outside the block.
 *
 * @param parent The directory in which the ephemeral directory is created in.
 * @param prefix The prefix of the ephemeral file
 * @param suffix The suffix of the ephemeral file
 * @param attributes An optional list of file attributes to set atomically when creating the directory.
 * @param block The code to execute with the created ephemeral directory
 */
inline fun <R> withEphemeralFile(
    parent: Path? = null,
    prefix: String = "",
    suffix: String = "",
    vararg attributes: FileAttribute<*> = arrayOf(),
    block: (Path) -> R,
): R {
    return EphemeralFile(parent, prefix, suffix, *attributes).use { tempDir ->
        block(tempDir.path)
    }
}
