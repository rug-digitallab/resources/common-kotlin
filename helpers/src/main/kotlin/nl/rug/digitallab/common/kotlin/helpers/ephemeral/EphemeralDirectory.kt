package nl.rug.digitallab.common.kotlin.helpers.ephemeral

import java.nio.file.Path
import java.nio.file.attribute.FileAttribute
import kotlin.io.path.*

/**
 * Defines an ephemeral directory resource, which is a directory that will automatically delete itself once the program exits.
 *
 * To use the resource, use [withEphemeralDirectory].
 *
 * @param parent The directory in which the ephemeral directory is created.
 * @param prefix The prefix of the ephemeral directory
 * @param attributes An optional list of file attributes to set atomically when creating the directory.
 */
open class EphemeralDirectory(
    parent: Path? = null, prefix: String = "", vararg attributes: FileAttribute<*> = arrayOf(),
) : AutoCloseable {
    val path = createTempDirectory(parent, prefix, *attributes).also {
        it.toFile().deleteOnExit()
    }

    @OptIn(ExperimentalPathApi::class)
    override fun close() {
        path.deleteRecursively()
    }
}

/**
 * A scope function to create an ephemeral directory and run the [block] with the created directory. The primary use
 * is to ensure that the directory is deleted once there is no reference to it. NOTE: these semantics are only ensured
 * as long as the block doesn't copy its parameter to a variable outside the block.
 *
 * @param parent The directory in which the ephemeral directory is created in.
 * @param prefix The prefix of the ephemeral directory
 * @param attributes An optional list of file attributes to set atomically when creating the directory.
 * @param block The code to execute with the created ephemeral directory
 */
inline fun <R> withEphemeralDirectory(
    parent: Path? = null,
    prefix: String = "",
    vararg attributes: FileAttribute<*> = arrayOf(),
    block: (Path) -> R,
): R {
    return EphemeralDirectory(parent, prefix, *attributes).use { tempDir ->
        block(tempDir.path)
    }
}
