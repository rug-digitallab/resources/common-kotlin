package nl.rug.digitallab.common.kotlin.helpers.smallrye.config

import io.smallrye.config.*

/**
 * Abstract [ConfigSourceInterceptor] implementation that behaves
 * very similar to the standard [FallbackConfigSourceInterceptor],
 * except that this implementation allows for multiple fallback
 * values. The semantics are otherwise the same.
 *
 * Any implementation of this class has to implement [getFallbacks],
 * returning a set of fallback values to try.
 */
abstract class FallbacksInterceptor : ConfigSourceInterceptor {
    /**
     * For a given config property name, return the set of fallback
     * names that need to be tried. All items in the set will be tried,
     * the result with the highest priority will be used.
     *
     * @param name The property name to find fallbacks for
     */
    internal abstract fun getFallbacks(name: String): Set<String>

    override fun getValue(context: ConfigSourceInterceptorContext, name: String): ConfigValue? =
        // We make a set of candidate names and search for configuration settings for
        // each of these names. Then, we prioritise results as follows:
        // 1. Values found for the original, unmodified name
        // 2. Values found for the modified name
        // 3. Default values for the original, unmodified name
        // 4. Default values for the modified name
        // Within each category, if multiple results are found, the "winning" configuration
        // option is chosen using standard Smallrye Config rules - based on config source
        // ordinal value and position within the config source.
        (setOf(name) + getFallbacks(name))
            // Find values for each possible name
            .map { context.proceed(it) }

            // Partition the results: first the non-default values, then the default values
            .partition { it?.configSourceName != "DefaultValuesConfigSource" }
            .toList()

            // Partition again: first the name matches, then the "alternate names"
            .flatMap { it.partition { a -> a?.name == name }.toList() }

            // Now we have 4 lists of possible values; we fold this list to distill
            // a final value.
            .fold(null as ConfigValue?) { acc, configValues ->
                // If we already found a config value in a higher-priority set,
                // we use that. Otherwise, we reduce the set of candidate values in
                // this priority using Smallrye Config rules.
                return@fold acc ?: configValues.reduceOrNull { original, candidate ->
                    if (ConfigValueComparison.compare(original, candidate) >= 0) original else candidate
                }
            }

    /**
     * This implementation is from io.smallrye.config.ConfigValue.CONFIG_SOURCE_COMPARATOR,
     * but that is sadly not public for unknown reasons. We need the same inheritance logic,
     * so we copy it here (Kotlin-ified).
     */
    object ConfigValueComparison : Comparator<ConfigValue> {
        override fun compare(original: ConfigValue?, candidate: ConfigValue?): Int {
            if(original == null) return -1
            if(candidate == null) return 1

            val ordinalCompare = original.configSourceOrdinal.compareTo(candidate.configSourceOrdinal)
            if(ordinalCompare != 0) return ordinalCompare

            val positionCompare = original.configSourcePosition.compareTo(candidate.configSourcePosition)
            if(positionCompare != 0) return positionCompare * -1

            // If both properties are profiled, prioritize the one with the most specific profile
            if (original.name.startsWith("%") && candidate.name.startsWith("%")) {
                val originalProfiles =
                    ProfileConfigSourceInterceptor.convertProfile(NameIterator(original.name).nextSegment.substring(1))
                val candidateProfiles =
                    ProfileConfigSourceInterceptor.convertProfile(NameIterator(candidate.name).nextSegment.substring(1))

                return originalProfiles.size.compareTo(candidateProfiles.size) * -1
            }

            return 0
        }
    }
}
