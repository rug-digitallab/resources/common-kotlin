package nl.rug.digitallab.common.kotlin.helpers.noarg

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

class NoArgConstructorTest {
    @Test
    fun `The NoArgConstructor annotation should be present on the NoArgClass`() {
        val annotation = NoArgClass::class.java.getAnnotation(NoArgConstructor::class.java)
        assertNotNull(annotation)
    }

    @Test
    fun `The NoArgConstructor annotation should be present on the NoArgDataClass`() {
        val annotation = NoArgDataClass::class.java.getAnnotation(NoArgConstructor::class.java)
        assertNotNull(annotation)
    }

    @NoArgConstructor
    class NoArgClass {
        val someProp: Any = Any()
    }

    @NoArgConstructor
    data class NoArgDataClass(val someParam: Any)
}
