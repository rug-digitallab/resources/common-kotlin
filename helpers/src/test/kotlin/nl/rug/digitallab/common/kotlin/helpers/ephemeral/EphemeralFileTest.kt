package nl.rug.digitallab.common.kotlin.helpers.ephemeral

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assumptions
import org.junit.jupiter.api.io.TempDir
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.*
import kotlin.io.path.*
import kotlin.test.Test
import kotlin.test.assertTrue

class EphemeralFileTest {
    @Test
    fun `The file EphemeralFile should not be present after it closes`() {
        val ephemeralFile = EphemeralFile()
        val ephemeralFilePath = ephemeralFile.path

        ephemeralFile.use {  }

        assertTrue {
            !ephemeralFilePath.exists()
        }
    }

    @Test
    fun `Classes inheriting EphemeralFile should have the underlying file be deleted after its used`() {
        val mockEphemeralFile = MockEphemeralFile()
        val mockEphemeralFilePath = mockEphemeralFile.path

        mockEphemeralFile.use {  }

        assertTrue {
            !mockEphemeralFilePath.exists()
        }
    }

    @Test
    fun `createEphemeralFile should create a temporary file`() {
        withEphemeralFile { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile()
            }
        }
    }

    @Test
    fun `createEphemeralFile should correctly attach a prefix to the name of the ephemeral file`() {
        val prefix = "prefix"
        withEphemeralFile(prefix = prefix) { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile() &&
                file.fileName.toString().startsWith(prefix)
            }
        }
    }

    @Test
    fun `createEphemeralFile should correctly attach a suffix to the name of the ephemeral file`() {
        val suffix = "suffix"
        withEphemeralFile(suffix = suffix) { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile() &&
                file.fileName.toString().endsWith(suffix)
            }
        }
    }

    @Test
    fun `createEphemeralFile should correctly place the ephemeral file in a parent directory`(@TempDir parent: Path) {
        withEphemeralFile(parent = parent) { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile() &&
                Files.isSameFile(parent, file.parent)
             }
        }
    }

    @Test
    fun `createEphemeralFile should correctly give the ephemeral file its supplied attributes`() {
        Assumptions.assumeTrue(FileSystems.getDefault().supportedFileAttributeViews().contains("posix"))

        val permissions = PosixFilePermissions.fromString("rwxr-x---")
        val attributes = arrayOf(PosixFilePermissions.asFileAttribute(permissions))

        withEphemeralFile(attributes = attributes) { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile()
            }
            assertEquals(file.getPosixFilePermissions(), permissions)
        }
    }

    @Test
    fun `createEphemeralFile should correctly create the ephemeral file when all its parameters are used`(@TempDir parent: Path) {
        Assumptions.assumeTrue(FileSystems.getDefault().supportedFileAttributeViews().contains("posix"))

        val prefix = "prefix"
        val suffix = "suffix"
        val permissions = PosixFilePermissions.fromString("rwxr-x---")
        val attributes = arrayOf(PosixFilePermissions.asFileAttribute(permissions))

        withEphemeralFile(parent, prefix, suffix, *attributes) { file ->
            assertTrue {
                file.exists() &&
                file.isRegularFile() &&
                Files.isSameFile(parent, file.parent) &&
                file.fileName.toString().startsWith(prefix) &&
                file.fileName.toString().endsWith(suffix)
            }
            assertEquals(file.getPosixFilePermissions(), permissions)
        }
    }

    @Test
    fun `withEphemeralFile should be able to run coroutines within a coroutine`() = runBlocking {
        val coroutine : suspend () -> Unit = {}
        withEphemeralFile {
            coroutine()
        }
    }

    class MockEphemeralFile : EphemeralFile()
}
