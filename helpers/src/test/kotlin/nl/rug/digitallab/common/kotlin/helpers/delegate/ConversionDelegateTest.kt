package nl.rug.digitallab.common.kotlin.helpers.delegate

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ConversionDelegateTest {
    @Test
    fun `Getting converted values should return the expected result`() {
        class TestType(
            val rawValue: String,
        ) {
            val uppercaseValue: String
                by ConversionDelegate(TestType::rawValue) { it.uppercase() }
        }

        Assertions.assertEquals("HELLOWORLD", TestType("HelloWorld").uppercaseValue)
    }

    @Test
    fun `Setting values should transform correctly to the wrapped field`() {
        class TestType(
            var rawValue: String,
        ) {
            var inverseValue: String
                by MutableConversionDelegate(TestType::rawValue, { it.reversed() }, { it.reversed() })
        }

        val newString = "NewWorld"
        val instance = TestType("HelloWorld")
        instance.inverseValue = newString.reversed()

        Assertions.assertEquals(newString, instance.rawValue)
    }
}
