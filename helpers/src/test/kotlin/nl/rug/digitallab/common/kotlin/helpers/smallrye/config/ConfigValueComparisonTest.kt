package nl.rug.digitallab.common.kotlin.helpers.smallrye.config

import io.smallrye.config.ConfigValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ConfigValueComparisonTest {
    @Test
    fun `Compare should return the correct integer for null values`() {
        val value = createConfigValue(100, 0)

        assertEquals(1, FallbacksInterceptor.ConfigValueComparison.compare(value, null))
        assertEquals(-1, FallbacksInterceptor.ConfigValueComparison.compare(null, value))
    }

    @Test
    fun `Compare should prioritize higher ordinal`() {
        val value1 = createConfigValue(100, 0)
        val value2 = createConfigValue(200, 0)

        assertEquals(-1, FallbacksInterceptor.ConfigValueComparison.compare(value1, value2))
        assertEquals(1, FallbacksInterceptor.ConfigValueComparison.compare(value2, value1))
    }

    @Test
    fun `Compare should prioritize lower position when ordinals are equal`() {
        val value1 = createConfigValue(100, 1)
        val value2 = createConfigValue(100, 0)

        assertEquals(-1, FallbacksInterceptor.ConfigValueComparison.compare(value1, value2))
        assertEquals(1, FallbacksInterceptor.ConfigValueComparison.compare(value2, value1))
    }

    @Test
    fun `Compare should return 0 for equal values`() {
        val value1 = createConfigValue(100, 0)
        val value2 = createConfigValue(100, 0)

        assertEquals(0, FallbacksInterceptor.ConfigValueComparison.compare(value1, value2))
        assertEquals(0, FallbacksInterceptor.ConfigValueComparison.compare(value2, value1))
    }

    @Test
    fun `Compare should prioritize the most specific profile`() {
        val value1 = createConfigValue(100, 0, "%prod,dev.test")
        val value2 = createConfigValue(100, 0, "%dev.test")

        assertEquals(-1, FallbacksInterceptor.ConfigValueComparison.compare(value1, value2))
        assertEquals(1, FallbacksInterceptor.ConfigValueComparison.compare(value2, value1))
    }

    private fun createConfigValue(ordinal: Int, position: Int, name: String = "test"): ConfigValue {
        return ConfigValue.builder()
            .withName(name)
            .withValue("value")
            .withConfigSourceName("source")
            .withConfigSourceOrdinal(ordinal)
            .withConfigSourcePosition(position)
            .build()
    }
}
