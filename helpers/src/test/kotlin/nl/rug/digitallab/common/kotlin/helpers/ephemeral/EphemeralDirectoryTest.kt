package nl.rug.digitallab.common.kotlin.helpers.ephemeral

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assumptions
import org.junit.jupiter.api.io.TempDir
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.PosixFilePermissions
import kotlin.io.path.*
import kotlin.test.*

class EphemeralDirectoryTest {
    @Test
    fun `The directory EphemeralDirectory should not be present after it closes`() {
        val ephemeralDirectory = EphemeralDirectory()
        val ephemeralDirectoryPath = ephemeralDirectory.path

        ephemeralDirectory.use {  }

        assertTrue {
            !ephemeralDirectoryPath.exists()
         }
    }

    @Test
    fun `Classes inheriting EphemeralDirectory should have the underlying directory be deleted after its used`() {
        val mockEphemeralDirectory = MockEphemeralDirectory()
        val mockEphemeralDirectoryPath = mockEphemeralDirectory.path

        mockEphemeralDirectory.use {  }

        assertTrue {
            !mockEphemeralDirectoryPath.exists()
        }
    }

    @Test
    fun `createEphemeralDirectory should create a temporary directory`() {
        withEphemeralDirectory { directory ->
            assertTrue {
                directory.exists() &&
                directory.isDirectory()
            }
        }
    }

    @Test
    fun `createEphemeralDirectory should correctly attach a prefix to the name of the ephemeral directory`() {
        val prefix = "prefix"
        withEphemeralDirectory(prefix = prefix) { directory ->
            assertTrue {
                directory.exists() &&
                directory.isDirectory() &&
                directory.fileName.toString().startsWith(prefix)
            }
        }
    }

    @Test
    fun `createEphemeralDirectory should correctly place the ephemeral directory in a parent directory`(@TempDir parent: Path) {
        withEphemeralDirectory(parent = parent) { directory ->
            assertTrue {
                directory.exists() &&
                directory.isDirectory() &&
                Files.isSameFile(parent, directory.parent)
            }
        }
    }

    @Test
    fun `createEphemeralDirectory should correctly give the ephemeral directory its supplied attributes`() {
        Assumptions.assumeTrue(FileSystems.getDefault().supportedFileAttributeViews().contains("posix"))

        val permissions = PosixFilePermissions.fromString("rwxr-x---")
        val attributes = arrayOf(PosixFilePermissions.asFileAttribute(permissions))

        withEphemeralDirectory(attributes = attributes) { directory ->
            assertTrue {
                directory.exists() &&
                directory.isDirectory()
            }
            assertEquals(directory.getPosixFilePermissions(), permissions)
         }
    }

    @Test
    fun `createEphemeralDirectory should correctly create the ephemeral directory when all its parameters are used`(@TempDir parent: Path) {
        Assumptions.assumeTrue(FileSystems.getDefault().supportedFileAttributeViews().contains("posix"))

        val prefix = "prefix"
        val permissions = PosixFilePermissions.fromString("rwxr-x---")
        val attributes = arrayOf(PosixFilePermissions.asFileAttribute(permissions))

        withEphemeralDirectory(parent, prefix, *attributes) { directory ->
            assertTrue {
                directory.exists() &&
                directory.isDirectory() &&
                Files.isSameFile(parent, directory.parent) &&
                directory.fileName.toString().startsWith(prefix)
            }
            assertEquals(directory.getPosixFilePermissions(), permissions)
        }
    }

    @Test
    fun `withEphemeralDirectory should be able to run coroutines within a coroutine`() = runBlocking {
        val coroutine : suspend () -> Unit = {}
        withEphemeralDirectory {
            coroutine()
         }
    }

    class MockEphemeralDirectory : EphemeralDirectory()
}
