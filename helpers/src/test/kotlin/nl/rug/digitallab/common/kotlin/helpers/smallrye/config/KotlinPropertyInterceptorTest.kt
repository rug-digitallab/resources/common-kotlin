package nl.rug.digitallab.common.kotlin.helpers.smallrye.config

import io.smallrye.config.ConfigMapping
import io.smallrye.config.PropertiesConfigSource
import io.smallrye.config.SmallRyeConfigBuilder
import io.smallrye.config.WithDefault
import io.smallrye.config.WithName
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class KotlinPropertyInterceptorTest {
    private inline fun <reified T> makeConfig(properties: Map<String, String>) : T
        = SmallRyeConfigBuilder()
        .addDefaultInterceptors()
        .addDiscoveredInterceptors()
        .withValidateUnknown(false)
        .withMapping(T::class.java)
        .withSources(PropertiesConfigSource(properties, "", 0))
        .addDiscoveredCustomizers()
        .build()
        .getConfigMapping(T::class.java)

    @Test
    fun `Property Interceptor should produce all combinations of stripped prefixes`() {
        val interceptor = KotlinPropertyInterceptor()

        Assertions.assertEquals(setOf("a"), interceptor.getFallbacks("a"))
        Assertions.assertEquals(setOf("get-a", "a"), interceptor.getFallbacks("get-a"))
        Assertions.assertEquals(
            setOf("get-a.get-b", "a.get-b", "get-a.b", "a.b"),
            interceptor.getFallbacks("get-a.get-b")
        )
    }

    @Test
    fun `Properties should be mapped properly`() {
        val configMap = listOf(
            "property",
            "function",
            "get-bad-name-property",
            "get-bad-name-function",
            "inline-property",
            "inline-function",
            "nested-property.property",
            "nested-property.function",
            "nested-function.property",
            "nested-function.function",
            "custom-value",
            "custom-function",
        ).associateWith { it }

        val configInstance = makeConfig<TestConfig>(configMap)

        // These assertions aren't pretty, but there is not really
        // an easier way since we need to map the strings to the fields
        // somehow.
        Assertions.assertEquals("property", configInstance.property)
        Assertions.assertEquals("function", configInstance.function())
        Assertions.assertEquals("get-bad-name-property", configInstance.getBadNameProperty)
        Assertions.assertEquals("get-bad-name-function", configInstance.getBadNameFunction())
        Assertions.assertEquals("inline-property", configInstance.inlineProperty.value)
        Assertions.assertEquals("inline-function", configInstance.inlineFunction().value)
        Assertions.assertEquals("nested-property.property", configInstance.nestedProperty.property)
        Assertions.assertEquals("nested-property.function", configInstance.nestedProperty.function())
        Assertions.assertEquals("nested-function.property", configInstance.nestedFunction().property)
        Assertions.assertEquals("nested-function.function", configInstance.nestedFunction().function())
        Assertions.assertEquals("default-value", configInstance.defaultValue)
        Assertions.assertEquals("custom-value", configInstance.customValue)
        Assertions.assertEquals("default-function", configInstance.defaultFunction())
        Assertions.assertEquals("custom-function", configInstance.customFunction())
    }

    @Test
    fun `Properties with the original name should take priority`() {
        // Since get-property corresponds to the original name, that is the expected
        // value
        val configMap = listOf(
            "property",
            "get-property"
        ).associateWith { it }

        val configInstance = makeConfig<PropertyConfig>(configMap)

        Assertions.assertEquals("get-property", configInstance.property)
    }

    @Test
    fun `Properties should not use 'alternate default'`() {
        // There are config fields "property" and "get-property", both with a
        // default value. The correct default value should be used.
        val configInstance = makeConfig<PropertyDefaultConfig>(emptyMap())

        Assertions.assertEquals("default", configInstance.property)
        Assertions.assertEquals("property-default", configInstance.default)
    }

    @ConfigMapping
    interface TestConfig {
        // Check whether properties and functions still map correctly
        val property: String
        fun function(): String

        // Check whether a preceding "get-" in the actual property name
        // is not incorrectly stripped
        val getBadNameProperty: String
        fun getBadNameFunction(): String

        // Test that value classes do actually work
        @get:WithName("inline-property")
        val inlineProperty: TestInline
        @WithName("inline-function")
        fun inlineFunction(): TestInline

        // Test that nested config maps work
        val nestedProperty: NestedConfig
        fun nestedFunction(): NestedConfig

        // Test that default values are properly ignored when
        // explicit value is present, but are used when no explicit
        // value is present.
        @get:WithDefault("default-value")
        val defaultValue: String
        @get:WithDefault("default-value")
        val customValue: String

        @WithDefault("default-function")
        fun defaultFunction(): String
        @WithDefault("default-function")
        fun customFunction(): String
    }

    @ConfigMapping
    interface NestedConfig {
        val property: String
        fun function(): String
    }

    @ConfigMapping
    interface PropertyConfig {
        val property: String
    }

    @ConfigMapping
    interface PropertyDefaultConfig {
        @get:WithDefault("default")
        val property: String

        @get:WithName("property")
        @get:WithDefault("property-default")
        val default: String
    }

    @JvmInline
    value class TestInline(val value: String)
}
