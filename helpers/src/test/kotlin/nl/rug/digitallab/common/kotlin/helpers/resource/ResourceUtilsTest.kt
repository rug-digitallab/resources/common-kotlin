package nl.rug.digitallab.common.kotlin.helpers.resource

import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralFile
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import java.io.FileNotFoundException
import java.io.InputStream
import java.net.URL
import java.nio.file.Paths
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.test.assertEquals

class ResourceUtilsTest {
    @TestFactory
    fun `Files in the resources root directory should be able to be loaded`(): List<DynamicTest> =
        listOf(
            "getResource" to { getResource("toplevel") },
            "getResource with Path" to { getResource(Path("toplevel")) },
            "getResourceAsStream" to { getResourceAsStream("toplevel") },
            "getResourceAsStream with Path" to { getResourceAsStream(Path("toplevel")) },
        ).map { (description, resource) ->
            DynamicTest.dynamicTest("Files in the resources root directory should be able to be loaded using $description") {
                assertEquals("This is a top level file", contentsOf(resource()))
            }
        }

    @TestFactory
    fun `Files within directories within the resources directory should be able to be loaded`(): List<DynamicTest> =
        listOf(
            "getResource" to { getResource("nested/file") },
            "getResource with Path" to { getResource(Path("nested/file")) },
            "getResourceAsStream" to { getResourceAsStream("nested/file") },
            "getResourceAsStream with Path" to { getResourceAsStream(Path("nested/file")) },
        ).map { (description, resource) ->
            DynamicTest.dynamicTest("Files within directories within the resources directory should be able to be loaded using $description") {
                assertEquals("This is a file within a directory", contentsOf(resource()))
            }
        }

    @TestFactory
    fun `Files that do not exist should throw an IllegalArgumentException`(): List<DynamicTest> =
        listOf(
            "getResource" to { getResource("doesnt/exist") },
            "getResource with Path" to { getResource(Path("doesnt/exist")) },
            "getResourceAsStream" to { getResourceAsStream("doesnt/exist") },
            "getResourceAsStream with Path" to { getResourceAsStream(Path("doesnt/exist")) },
        ).map { (description, resource) ->
            DynamicTest.dynamicTest("Files that do not exist should throw an IllegalArgumentException using $description") {
                assertThrows<IllegalArgumentException> {
                    resource()
                }
            }
        }

    @Test
    fun `Listing files in a resource directory should return the expected files`() {
        val resources = listResources("multiple")
        val expected = listOf("multiple/file1", "multiple/file2").map { getResource(it) }

        assertEquals(2, resources.size)
        assertEquals(expected, resources)
    }

    @Test
    fun `Getting a resource and converting it should return the same content`() {
        val resource = getResource("toplevel")

        assertEquals("This is a top level file", contentsOf(resource)) // URL
        assertEquals("This is a top level file", resource.asFile().readText()) // File
        assertEquals("This is a top level file", resource.asPath().readText()) // Path
    }

    @Test
    fun `Converting a URL that does not point to a file on the file system should throw a FileNotFoundException`() {
        val url = URL("https://notafile.com/onthefilesystem")

        assertThrows<FileNotFoundException> {
            url.asFile()
        }
    }

    @Test
    fun `Copying the contents of a URL to file should return the same content`() {
        val resource = getResource("toplevel")

        withEphemeralFile { file ->
            resource.copyToFile(file.toFile())
            assertEquals("This is a top level file", file.readText())
        }
    }

    private fun contentsOf(resource: Any): String = when (resource) {
        is URL -> resource.openStream().bufferedReader().use { it.readText() }
        is InputStream -> resource.bufferedReader().use { it.readText() }
        else -> throw IllegalArgumentException("Unknown resource type")
    }
}
