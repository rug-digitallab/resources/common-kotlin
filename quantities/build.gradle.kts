plugins {
    id("nl.rug.digitallab.gradle.plugin.kotlin.library")
}

fun DependencyHandler.hiddenImplementation(dependencyNotation: Any) {
    compileOnly(dependencyNotation)
    testImplementation(dependencyNotation)
}

dependencies {
    val quarkusVersion: String by project

    // We use the Kotlin naming interceptor in our tests, as well as the generic
    // conversion delegate for our delegates.
    implementation(project(":helpers"))

    // We include the Quarkus BOM as a hidden implementation to allow our hidden
    // dependencies to be of the same version of our current primary Quarkus version
    hiddenImplementation(platform("io.quarkus.platform:quarkus-bom:$quarkusVersion"))

    // We include these dependencies as "compile only", and then as implementation
    // in the test configuration. This way, we don't pollute the classpaths of a
    // user with the entirety of Quarkus when they don't use it and don't need it.
    // At the same time, we do (sneakily) include the actual implementations in the
    // library, so that when these dependencies are available, the integrations
    // are available and automatically picked up by the framework through DI.
    hiddenImplementation("io.smallrye.config:smallrye-config")
    hiddenImplementation("jakarta.persistence:jakarta.persistence-api")
    hiddenImplementation("io.quarkus:quarkus-jackson")

    // We need to explicitly include ASM again, since the Quarkus BOM has this as an
    // excluded transient dependency for Smallrye
    hiddenImplementation("org.ow2.asm:asm")

    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

tasks.test {
    useJUnitPlatform()

    dependsOn("jandex")
}
