package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import io.smallrye.config.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import kotlin.time.Duration

class DurationLongConverterTest {
    @ParameterizedTest
    @ValueSource(
        strings = [
            "45d", "1d 12h", "1.5d", "20h 30m", "1.546s", "1.546m", "25.12ms", "-12m", "12s", "140.884ms", "500us", "24ns",
            "5000us", "5000m", "Infinity", "-(1h 30m)", "PT9999999999999H", "-PT5M30S", "PT0.000000025S", "PT0.120300S",
            "PT30.500S", "PT30M30S", "PT24H0M20S", "PT0S"
        ]
    )
    fun `Smallrye Converter should parse valid values`(input: String) {
        val config = makeConfig(mapOf("test" to input))

        val configValue = config.getValue("test", Duration::class.java)
        val directValue = Duration.parse(input)
        assertEquals(directValue, configValue)
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "45 days", "45days", "1d 12hours", "Inf", "-1h 30m", "P5M30S"
        ]
    )
    fun `Smallrye Converter should throw on invalid values`(input: String) {
        val config = makeConfig(mapOf("test" to input))

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            config.getValue("test", Duration::class.java)
        }
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "45d", "1d 12h", "1.5d", "20h 30m", "1.546s", "1.546m", "25.12ms", "-12m", "12s", "140.884ms", "500us", "24ns",
            "5000us", "5000m", "Infinity", "-(1h 30m)", "PT9999999999999H", "-PT5M30S", "PT0.000000025S", "PT0.120300S",
            "PT30.500S", "PT30M30S", "PT24H0M20S", "PT0S"
        ]
    )
    fun `Smallrye Converter should parse values into configmap properties`(input: String) {
        val config = makeConfig(mapOf("root.test" to input))

        val configValue = config.getConfigMapping(DurationContainer::class.java)
        val directValue = Duration.parse(input)
        assertEquals(directValue, configValue.test)
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "45d", "1d 12h", "1.5d", "20h 30m", "1.546s", "1.546m", "25.12ms", "-12m", "12s", "140.884ms", "500us", "24ns",
            "5000us", "5000m", "Infinity", "-(1h 30m)", "PT9999999999999H", "-PT5M30S", "PT0.000000025S", "PT0.120300S",
            "PT30.500S", "PT30M30S", "PT24H0M20S", "PT0S"
        ]
    )
    fun `Smallrye Converter should parse values into configmap functions`(input: String) {
        val config = makeConfig(mapOf("root.fun-test" to input))

        val configValue = config.getConfigMapping(DurationContainer::class.java)
        val directValue = Duration.parse(input)
        assertEquals(directValue, configValue.funTest())
    }

    @ConfigMapping(prefix = "root")
    interface DurationContainer {
        @get:WithConverter(DurationLongConverter::class)
        @get:WithDefault("0ns")
        @get:WithName("test")
        val test: Duration

        @WithConverter(DurationLongConverter::class)
        @WithDefault("0ns")
        @WithName("fun-test")
        fun funTest(): Duration
    }

    /**
     * Simple convenience overload to make a config with a ByteSizeContainer mapping
     *
     * @param properties The map of configuration value names to their associated value.
     *
     * @return The [SmallRyeConfigBuilder] for the supplied [properties]
     */
    private fun makeConfig(properties: Map<String, String>) =
        makeConfig(properties, DurationContainer::class.java)
}
