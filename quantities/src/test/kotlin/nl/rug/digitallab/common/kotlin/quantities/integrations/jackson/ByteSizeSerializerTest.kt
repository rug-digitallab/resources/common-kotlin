package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import nl.rug.digitallab.common.kotlin.quantities.B
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class ByteSizeSerializerTest {
    private val mapper = ObjectMapper().registerKotlinModule().registerQuantitiesModule()
    
    @ParameterizedTest
    @ValueSource(longs = [1, 100, 100_000, 100_000_000, 99_999_999])
    fun `Jackson serializer outputs values in bytes`(input: Long) {
        val jacksonValue = mapper.writeValueAsString(input.B)
        val byteString = input.toString()
        
        Assertions.assertEquals(byteString, jacksonValue)
    }
}