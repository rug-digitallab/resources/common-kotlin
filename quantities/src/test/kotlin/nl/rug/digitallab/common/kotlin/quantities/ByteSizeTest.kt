package nl.rug.digitallab.common.kotlin.quantities

import nl.rug.digitallab.common.kotlin.quantities.exceptions.ByteSizeFormatException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.util.Locale

class ByteSizeTest {
    @ParameterizedTest(name = "Basic conversion functions should map {0} to itself")
    @ValueSource(longs = [1, 99, 100, 999, 1000, 1001, 1023, 1024, 1025])
    fun `Basic conversion functions should map to itself`(value: Long) {
        Assertions.assertEquals(value, value.B.B)
        Assertions.assertEquals(value, value.KB.KB)
        Assertions.assertEquals(value, value.KiB.KiB)
        Assertions.assertEquals(value, value.MB.MB)
        Assertions.assertEquals(value, value.MiB.MiB)
        Assertions.assertEquals(value, value.GB.GB)
        Assertions.assertEquals(value, value.GiB.GiB)
        Assertions.assertEquals(value, value.TB.TB)
        Assertions.assertEquals(value, value.TiB.TiB)
        Assertions.assertEquals(value, value.PB.PB)
        Assertions.assertEquals(value, value.PiB.PiB)

        // Special case for EB to prevent 64-bit Long overflow
        if(value <= 8) {
            Assertions.assertEquals(value, value.EB.EB)
            Assertions.assertEquals(value, value.EiB.EiB)
        }
    }

    @Test
    fun `Conversion between scales should work properly`() {
        // Byte -> Kilobyte
        Assertions.assertEquals(1, 1000.B.KB)
        Assertions.assertEquals(0, 1000.B.KiB)
        Assertions.assertEquals(1, 1024.B.KB)
        Assertions.assertEquals(1, 1024.B.KiB)

        // Kilobyte -> Megabyte
        Assertions.assertEquals(1, 1000.KB.MB)
        Assertions.assertEquals(0, 1000.KB.MiB)
        Assertions.assertEquals(1, 1024.KB.MB)
        Assertions.assertEquals(0, 1024.KB.MiB)
        Assertions.assertEquals(1, 1024.KiB.MB)
        Assertions.assertEquals(1, 1024.KiB.MiB)

        // Megabyte -> Gigabyte
        Assertions.assertEquals(1, 1000.MB.GB)
        Assertions.assertEquals(0, 1000.MB.GiB)
        Assertions.assertEquals(1, 1024.MB.GB)
        Assertions.assertEquals(0, 1024.MB.GiB)
        Assertions.assertEquals(1, 1024.MiB.GB)
        Assertions.assertEquals(1, 1024.MiB.GiB)

        // Gigabyte -> Terabyte
        Assertions.assertEquals(1, 1000.GB.TB)
        Assertions.assertEquals(0, 1000.GB.TiB)
        Assertions.assertEquals(1, 1024.GB.TB)
        Assertions.assertEquals(0, 1024.GB.TiB)
        Assertions.assertEquals(1, 1024.GiB.TB)
        Assertions.assertEquals(1, 1024.GiB.TiB)

        // Terabyte -> Petabyte
        Assertions.assertEquals(1, 1000.TB.PB)
        Assertions.assertEquals(0, 1000.TB.PiB)
        Assertions.assertEquals(1, 1024.TB.PB)
        Assertions.assertEquals(0, 1024.TB.PiB)
        Assertions.assertEquals(1, 1024.TiB.PB)
        Assertions.assertEquals(1, 1024.TiB.PiB)

        // Petabyte -> Exabyte
        Assertions.assertEquals(1, 1000.PB.EB)
        Assertions.assertEquals(0, 1000.PB.EiB)
        Assertions.assertEquals(1, 1024.PB.EB)
        Assertions.assertEquals(0, 1024.PB.EiB)
        Assertions.assertEquals(1, 1024.PiB.EB)
        Assertions.assertEquals(1, 1024.PiB.EiB)
    }

    @TestFactory
    fun `Formatting byte sizes should produce expected results`(): List<DynamicTest> {
        val tests = mapOf(
            // <ByteSize> to <precision> to <expected three output formats>
            0.B     to 1 to ExpectedFormats("0 B", "0 B", "0"),
            0.B     to 2 to ExpectedFormats("0 B", "0 B", "0"),
            27.B    to 1 to ExpectedFormats("27 B", "27 B", "27"),
            999.B   to 1 to ExpectedFormats("999 B", "999 B", "999"),
            1000.B  to 1 to ExpectedFormats("1.0 kB", "1000 B", "1.0K"),
            1023.B  to 0 to ExpectedFormats("1 kB", "1023 B", "1K"),
            1023.B  to 1 to ExpectedFormats("1.0 kB", "1023 B", "1.0K"),
            1024.B  to 0 to ExpectedFormats("1 kB", "1 KiB", "1K"),
            1024.B  to 1 to ExpectedFormats("1.0 kB", "1.0 KiB", "1.0K"),
            1728.B  to 1 to ExpectedFormats("1.7 kB", "1.7 KiB", "1.7K"),
            1728.B  to 2 to ExpectedFormats("1.73 kB", "1.69 KiB", "1.73K"),
            16.TB   to 1 to ExpectedFormats("16.0 TB", "14.6 TiB", "16.0T"),
            1855425871872.B  to 1 to ExpectedFormats("1.9 TB", "1.7 TiB", "1.9T"),
            Long.MAX_VALUE.B to 1 to ExpectedFormats("9.2 EB", "8.0 EiB", "9.2E"),
        )

        return tests.map { DynamicTest.dynamicTest("Formatting ${it.key.first.B} bytes with ${it.key.second} digits of precision") {
            val siResult = it.key.first.toFormattedString(ByteSize.Format.SI, it.key.second)
            val iecResult = it.key.first.toFormattedString(ByteSize.Format.IEC, it.key.second)
            val unixResult = it.key.first.toFormattedString(ByteSize.Format.UNIX, it.key.second)

            Assertions.assertEquals(it.value.si, siResult)
            Assertions.assertEquals(it.value.iec, iecResult)
            Assertions.assertEquals(it.value.unix, unixResult)
        }}
    }

    @TestFactory
    fun `Formatting localized byte sizes should produce localized results`(): List<DynamicTest> {
        val tests = mapOf(
            // <ByteSize> to <Locale> to <expected three output formats>
            0.B to Locale.UK to ExpectedFormats("0 B", "0 B", "0"),
            1.KiB to Locale.UK to ExpectedFormats("1.0 kB", "1.0 KiB", "1.0K"),
            1.KiB to Locale.forLanguageTag("nl-NL") to ExpectedFormats("1,0 kB", "1,0 KiB", "1,0K"),
        )

        return tests.map { DynamicTest.dynamicTest("Formatting ${it.key.first.B} bytes with ${it.key.second} digits of precision") {
            val siResult = it.key.first.toFormattedString(ByteSize.Format.SI, 1, it.key.second)
            val iecResult = it.key.first.toFormattedString(ByteSize.Format.IEC, 1, it.key.second)
            val unixResult = it.key.first.toFormattedString(ByteSize.Format.UNIX, 1, it.key.second)

            Assertions.assertEquals(it.value.si, siResult)
            Assertions.assertEquals(it.value.iec, iecResult)
            Assertions.assertEquals(it.value.unix, unixResult)
        }}
    }

    @TestFactory
    fun `Parsing valid byte sizes should produce accurate results`(): List<DynamicTest> {
        val tests = mapOf(
            // <input> to <ByteSize>
            "112" to 112.B,
            "123456789" to 123456789.B,
            "1B" to 1.B,
            "1 B" to 1.B,
            "1 b" to 1.B,
            " 123 " to 123.B,
            "1KB" to 1.KB,
            "1KiB" to 1.KiB,
            "1 kib" to 1.KiB,
            "1 k" to 1.KB,
            "123456 GB" to 123456.GB,
            "123_456 GB" to 123456.GB,
            "2M" to 2.MB,
        )

        return tests.map { DynamicTest.dynamicTest("Parsing ${it.key} into ${it.value.B} bytes") {
            Assertions.assertEquals(it.value, ByteSize.fromString(it.key))
        }}
    }

    @TestFactory
    fun `ByteSize should parse values ranging from byte to exibyte`(): List<DynamicTest> {
        val tests = mapOf(
            "1" to 1.B,
            "1B" to 1.B,
            "1K" to 1.KB,
            "1KB" to 1.KB,
            "1KiB" to 1.KiB,
            "1M" to 1.MB,
            "1MB" to 1.MB,
            "1MiB" to 1.MiB,
            "1G" to 1.GB,
            "1GB" to 1.GB,
            "1GiB" to 1.GiB,
            "1T" to 1.TB,
            "1TB" to 1.TB,
            "1TiB" to 1.TiB,
            "1P" to 1.PB,
            "1PB" to 1.PB,
            "1PiB" to 1.PiB,
            "1E" to 1.EB,
            "1EB" to 1.EB,
            "1EiB" to 1.EiB,
        )

        return tests.map { DynamicTest.dynamicTest("ByteSize ${it.key} should parse to ${it.value}") {
            Assertions.assertEquals(it.value, ByteSize.fromString(it.key))
        }}
    }

    @TestFactory
    fun `Parsing invalid byte sizes should throw an error`(): List<DynamicTest> {
        val tests = listOf(
            "321LI",
            "MB32",
            "MB",
            "4324333Q",
        )

        return tests.map { DynamicTest.dynamicTest("Parsing $it into invalid") {
            Assertions.assertThrows(ByteSizeFormatException::class.java) { ByteSize.fromString(it) }
        }}
    }

    @TestFactory
    fun `ByteSize objects can be correctly compared`(): List<DynamicTest> {
        val operators = listOf("<", "<=", ">", ">=", "==", "!=")

        val tests = listOf(
            1.B to 1.B,
            1.B to 2.B,
            2.B to 1.B,
            1.B to 1.KB,
            1.KB to 1.B,
            1.KB to 1.KB
        )

        return tests.flatMap { (a, b) ->
            operators.map { operator ->
                DynamicTest.dynamicTest("Comparing $a and $b with $operator should produce ${compare(a.B, b.B, operator)}") {
                    Assertions.assertEquals(compare(a.B, b.B, operator), compare(a, b, operator))
                }
            }
        }
    }

    @TestFactory
    fun `Formatting byte size using format should produce the expected result`(): List<DynamicTest> {
        data class TestCase(val format: String, val size: ByteSize, val expected: String)

        val tests = listOf(
            TestCase("%s", 0.B, "0 B"),
            TestCase("%s", 10.MB, "10.0 MB"),
            TestCase("%#s", 10.MiB, "10.0 MiB"),
            TestCase("%.2s", 10.MiB, "10.49 MB"),
            TestCase("%#.2s", 10.MiB, "10.00 MiB"),
        )

        return tests.map { testcase ->
            DynamicTest.dynamicTest("Formatting ${testcase.size} with ${testcase.format} should produce ${testcase.expected}") {
                Assertions.assertEquals(testcase.expected, testcase.format.format(testcase.size))
            }
        }
    }

    private fun <T : Comparable<T>> compare(a: T, b: T, operator: String): Boolean {
        return when (operator) {
            "<" -> a < b
            "<=" -> a <= b
            ">" -> a > b
            ">=" -> a >= b
            "==" -> a == b
            "!=" -> a != b
            else -> throw IllegalArgumentException("Invalid operator")
        }
    }

    private class ExpectedFormats(val si: String, val iec: String, val unix: String)
}
