package nl.rug.digitallab.common.kotlin.quantities

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ByteSizeDelegateTest {
    @Test
    fun `Default ByteSize delegate should convert to bytes`() {
        class TestType(
            val readonlyRawBytes: Long,
            var mutableRawBytes: Long,
        ) {
            val readonlyParsed by wrapByteSize(TestType::readonlyRawBytes)
            var mutableParsed by wrapByteSize(TestType::mutableRawBytes)
        }

        val instance = TestType(100 * 1024, 100_000)
        Assertions.assertEquals(100, instance.readonlyParsed.KiB)
        Assertions.assertEquals(100, instance.mutableParsed.KB)

        instance.mutableParsed = 123.MiB
        Assertions.assertEquals(123, instance.mutableParsed.MiB)
        Assertions.assertEquals(123 * 1024 * 1024, instance.mutableRawBytes)

        instance.mutableRawBytes = 234_000
        Assertions.assertEquals(234, instance.mutableParsed.KB)
    }

    @Test
    fun `Custom ByteSize delegate should convert to custom magnitude`() {
        class TestType(
            val readonlyRawMegaBytes: Long,
            var mutableRawKibiBytes: Long,
        ) {
            val readonlyParsed by wrapByteSize(TestType::readonlyRawMegaBytes, ByteSize.Magnitude.MB)
            var mutableParsed by wrapByteSize(TestType::mutableRawKibiBytes, ByteSize.Magnitude.KiB)
        }

        val instance = TestType(100, 100)
        Assertions.assertEquals(100, instance.readonlyParsed.MB)
        Assertions.assertEquals(100, instance.mutableParsed.KiB)
        Assertions.assertEquals(100 * 1024, instance.mutableParsed.B)

        instance.mutableParsed = 123.MiB
        Assertions.assertEquals(123, instance.mutableParsed.MiB)
        Assertions.assertEquals(123 * 1024, instance.mutableRawKibiBytes)

        instance.mutableRawKibiBytes = 234 * 1024
        Assertions.assertEquals(234, instance.mutableParsed.MiB)
    }
}
