package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import io.smallrye.config.PropertiesConfigSource
import io.smallrye.config.SmallRyeConfig
import io.smallrye.config.SmallRyeConfigBuilder

/**
 * Helper function to make a [SmallRyeConfigBuilder] from a map of configuration value names to their value.
 *
 * @param T The type of the mapping we are making the config for
 * @param properties The map of configuration value names to their associated value.
 *
 * @return The [SmallRyeConfigBuilder] for the supplied [properties]
 */
fun <T> makeConfig(properties: Map<String, String>, mapping: Class<T>): SmallRyeConfig = SmallRyeConfigBuilder()
    .addDefaultInterceptors()
    .addDiscoveredConverters()
    .withMapping(mapping)
    .withSources(PropertiesConfigSource(properties, "", 0))
    .withValidateUnknown(false)
    .addDiscoveredInterceptors()
    .build()
