package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import io.smallrye.config.*
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class ByteSizeConverterTest {
    @ParameterizedTest
    @ValueSource(strings = ["1B", "100 MB", "100_000 KiB", "100M"])
    fun `Smallrye Converter should parse valid values`(input: String) {
        val config = makeConfig(mapOf("test" to input))

        val configValue = config.getValue("test", ByteSize::class.java)
        val directValue = ByteSize.fromString(input)
        Assertions.assertEquals(directValue, configValue)
    }

    @ParameterizedTest
    @ValueSource(strings = ["321LI", "MB32", "MB", "432432432Q"])
    fun `Smallrye Converter should throw on invalid values`(input: String) {
        val config = makeConfig(mapOf("test" to input))

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            config.getValue("test", ByteSize::class.java)
        }
    }

    @ParameterizedTest
    @ValueSource(strings = ["1B", "100 MB", "100_000 KiB", "100M"])
    fun `Smallrye Converter should parse values into configmap properties`(input: String) {
        val config = makeConfig(mapOf("root.test" to input))

        val configValue = config.getConfigMapping(ByteSizeContainer::class.java)
        val directValue = ByteSize.fromString(input)
        Assertions.assertEquals(directValue.B, configValue.test.B)
    }

    @ParameterizedTest
    @ValueSource(strings = ["1B", "100 MB", "100_000 KiB", "100M"])
    fun `Smallrye Converter should parse values into configmap functions`(input: String) {
        val config = makeConfig(mapOf("root.fun-test" to input))

        val configValue = config.getConfigMapping(ByteSizeContainer::class.java)
        val directValue = ByteSize.fromString(input)
        Assertions.assertEquals(directValue.B, configValue.funTest().B)
    }

    @ConfigMapping(prefix = "root")
    interface ByteSizeContainer {
        @get:WithConverter(ByteSizeLongConverter::class)
        @get:WithDefault("0")
        @get:WithName("test")
        val test: ByteSize

        @WithDefault("0")
        @WithConverter(ByteSizeLongConverter::class)
        @WithName("fun-test")
        fun funTest(): ByteSize
    }

    /**
     * Simple convenience overload to make a config with a ByteSizeContainer mapping
     *
     * @param properties The map of configuration value names to their associated value.
     *
     * @return The [SmallRyeConfigBuilder] for the supplied [properties]
     */
    private fun makeConfig(properties: Map<String, String>) =
        makeConfig(properties, ByteSizeContainer::class.java)
}
