package nl.rug.digitallab.common.kotlin.quantities.exceptions

/**
 * This exception indicates a parsing failure of the ByteSize
 * string. The reason will be included
 *
 * @param message The message to the user indicating what went wrong
 * @param input The original input for which parsing failed
 */
class ByteSizeFormatException(
    message: String,
    val input: String,
) : IllegalArgumentException(message)
