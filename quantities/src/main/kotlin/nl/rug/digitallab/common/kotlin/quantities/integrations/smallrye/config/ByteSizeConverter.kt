package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import org.eclipse.microprofile.config.spi.Converter

/**
 * Simple [Converter] to allow parsing of [ByteSize] properties
 * in the Smallrye configuration. This converter is automatically
 * registered through META-INF.
 *
 * WARNING: Do not use this converter to annotate a field in a
 * ConfigMapping! Instead, use [ByteSizeLongConverter]
 */
class ByteSizeConverter : Converter<ByteSize> {
    override fun convert(input: String?): ByteSize {
        return ByteSize.fromString(input ?: "")
    }
}

/**
 * Simple [Converter] to allow parsing of [ByteSize] properties
 * that are represented by [Long]s in the Smallrye configuration.
 * This converter is designed to be used on ConfigMap entities
 * and needs to be included manually and explicitly.
 */
class ByteSizeLongConverter : Converter<Long> {
    override fun convert(input: String?): Long {
        return ByteSize.fromString(input ?: "").B
    }
}
