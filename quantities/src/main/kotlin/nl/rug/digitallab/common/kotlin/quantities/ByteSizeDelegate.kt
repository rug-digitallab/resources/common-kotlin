package nl.rug.digitallab.common.kotlin.quantities

import nl.rug.digitallab.common.kotlin.helpers.delegate.ConversionDelegate
import nl.rug.digitallab.common.kotlin.helpers.delegate.MutableConversionDelegate
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

/**
 * Provides a delegate that wraps the provided [Long] field in a [ByteSize]
 * instance. Any reads will read the source [Long] field and convert it into
 * a [ByteSize] of the provided [magnitude] - by default it will assume the
 * source field to be in bytes.
 *
 * @param backingProperty The backing [Long] property
 * @param magnitude The magnitude of the value in that property
 */
fun <T> wrapByteSize(
    backingProperty: KProperty1<T, Long>,
    magnitude: ByteSize.Magnitude = ByteSize.Magnitude.B,
) = ConversionDelegate(backingProperty, magnitude.fromNumber)

/**
 * Provides a delegate that wraps the provided [Long] field in a [ByteSize]
 * instance. Any reads and writes will be directed to the source [Long] field
 * as "backing store" of the provided [magnitude] - by default it will assume
 * the source field to be in bytes.
 *
 * @param backingProperty The backing [Long] property
 * @param magnitude The magnitude of the value in that property
 */
fun <T> wrapByteSize(
    backingProperty: KMutableProperty1<T, Long>,
    magnitude: ByteSize.Magnitude = ByteSize.Magnitude.B,
) = MutableConversionDelegate(backingProperty, magnitude.fromNumber, magnitude.toLong)
