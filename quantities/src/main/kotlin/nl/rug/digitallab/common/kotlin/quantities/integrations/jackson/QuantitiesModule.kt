package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import nl.rug.digitallab.common.kotlin.quantities.ByteSize

/**
 * Simple Module for Jackson to register the deserializers
 * in this library.
 *
 * Note: use [registerQuantitiesModule] to directly add this
 * module to an [ObjectMapper]
 */
class QuantitiesModule : SimpleModule() {
    init {
        addDeserializer(ByteSize::class.java, ByteSizeDeserializer)
        addSerializer(ByteSize::class.java, ByteSizeSerializer)
    }
}

/**
 * Helper function to automatically register the [QuantitiesModule]
 * and thus all deserializers in this library to an [ObjectMapper]
 */
fun ObjectMapper.registerQuantitiesModule() = this.registerModule(QuantitiesModule())!!
