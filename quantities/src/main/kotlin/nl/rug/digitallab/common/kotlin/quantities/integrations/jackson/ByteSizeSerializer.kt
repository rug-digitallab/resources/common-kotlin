package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import nl.rug.digitallab.common.kotlin.quantities.ByteSize

/**
 * Jackson Serializer for [ByteSize] instances. This is a very simple
 * serializer, writing any value to a number in bytes. This is compatible
 * with the associated [ByteSizeDeserializer].
 * 
 * Note: use this serializer by using the [QuantitiesModule], which can
 * be registered via [registerQuantitiesModule].
 */
object ByteSizeSerializer : StdSerializer<ByteSize>(ByteSize::class.java) {
    private fun readResolve(): Any = ByteSizeSerializer
    
    override fun serialize(value: ByteSize?, gen: JsonGenerator, provider: SerializerProvider) {
        when (value) {
            null -> gen.writeNull()
            else -> gen.writeNumber(value.B)
        }
    }
}