package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.inject.Singleton

/**
 * An [ObjectMapperCustomizer] that will automatically customize
 * [ObjectMapper] instances through Quarkus CDI. This way, the user
 * does not need to call [registerQuantitiesModule] manually.
 */
@Singleton
class QuantitiesCustomizer : ObjectMapperCustomizer {
    // This customizer should be the lowest priority, so it is
    // executed _last_. This means that it is executed _after_
    // the Kotlin module is registered, so any customizers we
    // add here are not overridden by the Kotlin module.
    override fun priority() = ObjectMapperCustomizer.MINIMUM_PRIORITY
    override fun customize(objectMapper: ObjectMapper) {
        objectMapper.registerQuantitiesModule()
    }
}
