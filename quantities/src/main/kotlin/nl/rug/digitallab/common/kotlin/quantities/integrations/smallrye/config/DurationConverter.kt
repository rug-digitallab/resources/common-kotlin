package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import org.eclipse.microprofile.config.spi.Converter
import kotlin.time.Duration

/**
 * Simple [Converter] to allow parsing of [Duration] properties in the Smallrye configuration. This converter is automatically
 * registered through META-INF.
 *
 * WARNING: Do not use this converter to annotate a field in a ConfigMapping! Instead, use [DurationLongConverter]
 */
class DurationConverter : Converter<Duration> {
    /**
     * Converts a string to a [Duration]
     *
     * @param input The string to convert.
     *
     * @return The converted [Duration].
     */
    override fun convert(input: String?): Duration {
        return Duration.parse(input ?: "0ns")
    }
}

/**
 * Simple [Converter] to allow parsing of [Duration] properties that are represented by [Long]s in the Smallrye configuraiton.
 * This converter is designed to be used on ConfigMap entities and needs to be included manually and explicitly.
 */
class DurationLongConverter: Converter<Long> {
    /**
     * Converts a string to an internal representation of a [Duration].
     *
     * Kotlin has an internal optimisation where they shift right the internal long value of a duration by 1.
     * They do this to mimic an unsigned long, which is a more fitting representation for a duration.
     * In order to convert to a long, we need to counteract this shift by shifting left by 1.
     *
     * However, in the case of [Duration.INFINITE], we do not shift left, as we should pass the exact internal
     * representation, as [Duration.INFINITE] is a symbolic value.
     *
     * @param input The string to convert.
     *
     * @return The converted internal representation of the [Duration].
     */
    override fun convert(input: String?): Long {
        val nanoseconds = Duration.parse(input ?: "0ns").inWholeNanoseconds

        return if (nanoseconds == Duration.INFINITE.inWholeNanoseconds) {
            nanoseconds
        } else {
            nanoseconds shl 1
        }
    }
}
