package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.exceptions.ByteSizeFormatException

/**
 * Jackson Deserializer for [ByteSize] instances. This is a very simple
 * deserializer, accepting string or number inputs. Strings are converted
 * through [ByteSize.fromString], and numbers are assumed to be in bytes.
 *
 * Note: use this deserializer by using the [QuantitiesModule], which can
 * be registered via [registerQuantitiesModule].
 */
object ByteSizeDeserializer : StdDeserializer<ByteSize>(ByteSize::class.java) {
    private fun readResolve(): Any = ByteSizeDeserializer
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): ByteSize {
        when(p.currentToken) {
            // Strings are interpreted using the dedicated parsing function
            JsonToken.VALUE_STRING -> {
                try {
                    return ByteSize.fromString(p.valueAsString)
                } catch(ex: ByteSizeFormatException) {
                    ctxt.handleWeirdStringValue(ByteSize::class, p.valueAsString, ex.message, ex)
                }
            }
            
            // Numbers are interpreted as having the byte unit
            JsonToken.VALUE_NUMBER_INT -> {
                return p.valueAsLong.B
            }
            
            // Otherwise, we throw an error
            else -> ctxt.handleUnexpectedToken(ByteSize::class, p)
        }
    }
}
