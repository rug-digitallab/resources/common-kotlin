package nl.rug.digitallab.common.kotlin.quantities.integrations.jakarta

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.ByteSize

/**
 * Simple [AttributeConverter] to allow persisting [ByteSize] columns
 * in a database table using Jakarta. The converter is automatically
 * available through the annotation.
 *
 * By default, Jakarta will use the "unboxed" [Long] type that is
 * exposed by the Kotlin compiler in the JVM version of a class.
 * However, in some cases, such as for nullable properties, the
 * unboxing doesn't happen, and then this converter will be picked up.
 *
 * NOTE: It is not needed to explicitly annotate [ByteSize] fields
 * with this converter.
 */
@Converter(autoApply = true)
class ByteSizeConverter : AttributeConverter<ByteSize, Long> {
    override fun convertToDatabaseColumn(size: ByteSize?): Long? = size?.B
    override fun convertToEntityAttribute(size: Long?): ByteSize? = size?.B
}
