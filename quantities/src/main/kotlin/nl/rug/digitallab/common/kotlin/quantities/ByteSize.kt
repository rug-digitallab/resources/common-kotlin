package nl.rug.digitallab.common.kotlin.quantities

import nl.rug.digitallab.common.kotlin.quantities.exceptions.ByteSizeFormatException
import java.util.Formattable
import java.util.FormattableFlags
import java.util.Formatter
import java.util.Locale

import kotlin.math.pow

/**
 * This class encapsulates some "byte size" quantity. It is a value class containing a [Long]
 * as internal representation of the number of bytes. Using this class is made easy through
 * utility properties both on [ByteSize] itself as well as on [Number]. This allows for explicit
 * but automatic conversion between different units of byte sizes as well.
 *
 * For example:
 * ```kotlin
 * val size = 100.KB
 * assert size.B == 100_000
 * assert size.KiB == 97
 * ```
 *
 * This class provides helper functions for conversion to and from a human-readable [String],
 * and as such serves as the canonical byte size representation for the Digital Lab.
 *
 * @property bytes Internal storage of the actual byte size.
 */
@JvmInline
value class ByteSize(private val bytes: Long) : Formattable, Comparable<ByteSize> {
    // Implement support for comparing byte sizes
    override operator fun compareTo(other: ByteSize): Int = bytes.compareTo(other.bytes)

    // Various conversions back to [Long]s.
    val B   get() = bytes
    val KB  get() = bytes / 1000
    val KiB get() = bytes / 1024
    val MB  get() = bytes / 1000 / 1000
    val MiB get() = bytes / 1024 / 1024
    val GB  get() = bytes / 1000 / 1000 / 1000
    val GiB get() = bytes / 1024 / 1024 / 1024
    val TB  get() = bytes / 1000 / 1000 / 1000 / 1000
    val TiB get() = bytes / 1024 / 1024 / 1024 / 1024
    val PB  get() = bytes / 1000 / 1000 / 1000 / 1000 / 1000
    val PiB get() = bytes / 1024 / 1024 / 1024 / 1024 / 1024
    val EB  get() = bytes / 1000 / 1000 / 1000 / 1000 / 1000 / 1000
    val EiB get() = bytes / 1024 / 1024 / 1024 / 1024 / 1024 / 1024

    // Human-readable Strings

    /**
     * Converts the current [ByteSize] to a [String] in the default [Format.SI] format.
     */
    override fun toString(): String {
        return toFormattedString(Format.SI)
    }

    /**
     * Implementation of the [Formattable] interface to allow for automatic formatting
     * of [ByteSize]s through the "%s" specification in a format string.
     *
     * Will produce an [Format.SI] representation with the given precision by default,
     * but supports the alternate [Format.IEC] representation.
     *
     * It is not recommended to use this function directly. Instead, use [toFormattedString].
     */
    override fun formatTo(formatter: Formatter, flags: Int, width: Int, precision: Int) {
        val format =
            if((flags and FormattableFlags.ALTERNATE) == FormattableFlags.ALTERNATE)
                Format.IEC
            else
                Format.SI

        // Precision is -1 when not specified, use 1 in that case
        val actualPrecision = if(precision == -1) 1 else precision

        formatter.format(toFormattedString(format, actualPrecision, formatter.locale()))
    }

    /**
     * Converts the current [ByteSize] to a human-readable [String] representation,
     * in the given [format] and [locale], with the given [precision].
     *
     * Refer to [Format] for documentation on the formats. [precision] indicates the
     * number of digits behind the decimal point if relevant. The decimal point is
     * localised based on the [locale].
     *
     * When [precision] != 0, the output of this function is not compatible with
     * [fromString], as that only accepts integer values.
     *
     * When a specific unit (such as KiB or GB) is desired, instead of a dynamic
     * unit based on the value, it is better to explicitly convert to that unit and
     * print by hand.
     *
     * @param format The byte size representation format to produce
     * @param precision The number of decimals, if relevant
     * @param locale The locale used for printing the numbers
     */
    fun toFormattedString(format: Format, precision: Int = 1, locale: Locale? = null): String {
        val names = when(format) {
            Format.SI -> listOf(" B", " kB", " MB", " GB", " TB", " PB", " EB")
            Format.IEC -> listOf(" B", " KiB", " MiB", " GiB", " TiB", " PiB", " EiB")
            Format.UNIX -> listOf("", "K", "M", "G", "T", "P", "E")
        }
        val multiplier = when(format) {
            Format.IEC -> 1024
            else -> 1000
        }

        // We will choose the largest possible unit based on the current byte size:
        // As long as possible, we will divide the current byte size by the multiplier
        // associated with the format. At the same time, we will make steps in the
        // iterator of our unit name list.
        var byteRepresentation = bytes.toDouble()
        val currentName = names.iterator()

        // We only want to print decimal numbers when we have divided at least once,
        // since "1.0 B" is nonsensical. So we temporarily set the precision to 0 until
        // we perform a division.
        var actualPrecision = 0

        // Repeatedly divide as long as we can
        while(byteRepresentation >= multiplier) {
            byteRepresentation /= multiplier
            currentName.next()
            actualPrecision = precision
        }

        // Format string: print the current [Double] with [actualPrecision] number of decimals
        return String.format(locale, "%.${actualPrecision}f%s", byteRepresentation, currentName.next())
    }

    companion object {
        /**
         * Tries to convert the given [input] to a [ByteSize]. Supports all [Format]s
         * supported elsewhere in the code. Only accepts positive integer values
         * (so no "1.5KB" or "-12MB").
         *
         * Parsing is quite lenient on the input format, in an attempt to be suitable
         * for user input. The implementation will simply read all characters until the
         * first letter in the input, and consider that as the "number" value. The remainder
         * of the input is considered to be the unit, and is matched case-insensitively
         * against all accepted formats. Whitespace is ignored.
         *
         * In order to prevent confusion between locales, "_" is the only allowed
         * "grouping separator" (aka "thousands separator"): "100,000KB" is therefore
         * not allowed, but "100_000KB" is.
         *
         * @param input The string to parse
         * @return [ByteSize] The parsed result, with value "-1 B" on parse failure.
         * @throws ByteSizeFormatException When the input is invalid
         */
        @Throws(ByteSizeFormatException::class)
        fun fromString(input: String): ByteSize {
            // Extract first and second part of input string
            val number = input.takeWhile { !it.isLetter() }.filter { !it.isWhitespace() }.filterNot { it == '_' }
            val suffix = input.dropWhile { !it.isLetter() }.filter { !it.isWhitespace() }.lowercase()

            val bytes = number.toLongOrNull()
            val (format, exp) = when(suffix.lowercase()) {
                "" -> Format.SI to 0
                "b" -> Format.SI to 0
                "kb" -> Format.SI to 1
                "kib" -> Format.IEC to 1
                "k" -> Format.UNIX to 1
                "mb" -> Format.SI to 2
                "mib" -> Format.IEC to 2
                "m" -> Format.UNIX to 2
                "gb" -> Format.SI to 3
                "gib" -> Format.IEC to 3
                "g" -> Format.UNIX to 3
                "tb" -> Format.SI to 4
                "tib" -> Format.IEC to 4
                "t" -> Format.UNIX to 4
                "pb" -> Format.SI to 5
                "pib" -> Format.IEC to 5
                "p" -> Format.UNIX to 5
                "eb" -> Format.SI to 6
                "eib" -> Format.IEC to 6
                "e" -> Format.UNIX to 6
                else -> Format.SI to -1 // Indicate failure
            }
            val multiplier = when(format) {
                Format.IEC -> 1024
                else -> 1000
            }

            // We could not determine the unit or were not able to parse the number
            if(bytes == null)
                throw ByteSizeFormatException("Could not find a number in the input", input)
            if(exp == -1)
                throw ByteSizeFormatException("Invalid byte size suffix", input)

            // This long conversion is sadly needed because Kotlin does not offer
            // exponentiation on integer types.
            val power = multiplier.toDouble().pow(exp).toLong()
            return ByteSize(bytes * power)
        }
    }

    /**
     * Specification of supported formats for parsing and printing human-readable
     * byte size strings.
     */
    enum class Format {
        /**
         * The SI format is 1000-based, and is the common default (kB, MB, ...)
         */
        SI,

        /**
         * The IEC format is 1024-based, and is the default on Windows systems (KiB, MiB, ...)
         */
        IEC,

        /**
         * The Unix format is 1000-based, and is used in some Unix CLI tools (K, M, ...)
         */
        UNIX,
    }

    /**
     * Listing of all supported [ByteSize] magnitudes, including their conversion
     * functions to and from [ByteSize]. This helps dynamically producing [ByteSize]
     * when both the raw value and its magnitude are "variable", without going through
     * an intermediate string representation.
     */
    enum class Magnitude(
        val fromNumber: (Number) -> ByteSize,
        val toLong: (ByteSize) -> Long,
    ) {
        B(Number::B, ByteSize::B),
        KB(Number::KB, ByteSize::KB),
        KiB(Number::KiB, ByteSize::KiB),
        MB(Number::MB, ByteSize::MB),
        MiB(Number::MiB, ByteSize::MiB),
        GB(Number::GB, ByteSize::GB),
        GiB(Number::GiB, ByteSize::GiB),
        TB(Number::TB, ByteSize::TB),
        TiB(Number::TiB, ByteSize::TiB),
        PB(Number::PB, ByteSize::PB),
        PiB(Number::PiB, ByteSize::PiB),
        EB(Number::EB, ByteSize::EB),
        EiB(Number::EiB, ByteSize::EiB),
    }
}

// Conversion extension properties for [Number] to [ByteSize]
val Number.B   get() = ByteSize(this.toLong())
val Number.KB  get() = ByteSize(this.toLong() * 1000)
val Number.KiB get() = ByteSize(this.toLong() * 1024)
val Number.MB  get() = ByteSize(this.toLong() * 1000 * 1000)
val Number.MiB get() = ByteSize(this.toLong() * 1024 * 1024)
val Number.GB  get() = ByteSize(this.toLong() * 1000 * 1000 * 1000)
val Number.GiB get() = ByteSize(this.toLong() * 1024 * 1024 * 1024)
val Number.TB  get() = ByteSize(this.toLong() * 1000 * 1000 * 1000 * 1000)
val Number.TiB get() = ByteSize(this.toLong() * 1024 * 1024 * 1024 * 1024)
val Number.PB  get() = ByteSize(this.toLong() * 1000 * 1000 * 1000 * 1000 * 1000)
val Number.PiB get() = ByteSize(this.toLong() * 1024 * 1024 * 1024 * 1024 * 1024)
val Number.EB  get() = ByteSize(this.toLong() * 1000 * 1000 * 1000 * 1000 * 1000 * 1000)
val Number.EiB get() = ByteSize(this.toLong() * 1024 * 1024 * 1024 * 1024 * 1024 * 1024)
