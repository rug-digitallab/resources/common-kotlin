plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    testImplementation(project(":quantities"))
    testImplementation(project(":helpers"))

    testImplementation("io.quarkus:quarkus-jackson")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    testImplementation("io.quarkus:quarkus-jdbc-mariadb")
    testImplementation("io.quarkus:quarkus-reactive-mysql-client")
    testImplementation("io.quarkus:quarkus-test-hibernate-reactive-panache")
    testImplementation("io.quarkus:quarkus-hibernate-reactive-panache-kotlin")
}
