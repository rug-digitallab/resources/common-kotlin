package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.digitallab.common.kotlin.quantities.MB
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class SmallryeByteSizeConverterTest {
    @Inject
    private lateinit var config: ByteSizeContainer

    @Test
    fun `Smallrye Converters should be registered automatically`() {
        Assertions.assertEquals(100.MB, config.byteSizeProperty)
        Assertions.assertEquals(100.KB, config.byteSizeFunction())
    }

    @ConfigMapping(prefix = "test")
    interface ByteSizeContainer {
        @get:WithName("byte-size-property")
        @get:WithConverter(ByteSizeLongConverter::class)
        val byteSizeProperty: ByteSize

        @WithConverter(ByteSizeLongConverter::class)
        @WithName("byte-size-function")
        fun byteSizeFunction(): ByteSize
    }
}
