package nl.rug.digitallab.common.kotlin.quantities.integrations.jakarta

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.KB
import org.junit.jupiter.api.Test
import java.util.UUID

@QuarkusTest
class JakartaByteSizeConverterTest {
    @Inject
    lateinit var repository: TestRepository

    @Test
    @RunOnVertxContext
    fun `Creating and retrieving an entity should yield the same byte size`(asserter: TransactionalUniAsserter) {
        val entity = TestEntity(100.KB, 123.KB)

        asserter.execute<TestEntity> { repository.persist(entity) }
            .assertEquals({ repository.findById(entity.id).map { it.size } }, entity.size)
            .assertEquals({ repository.findById(entity.id).map { it.optionalSize } }, entity.optionalSize)
    }

    @Test
    @RunOnVertxContext
    fun `Creating and retrieving a null for ByteSize should work`(asserter: TransactionalUniAsserter) {
        val entity = TestEntity(100.KB, null)

        asserter.execute<TestEntity> { repository.persist(entity) }
            .assertEquals({ repository.findById(entity.id).map { it.size } }, entity.size)
            .assertEquals({ repository.findById(entity.id).map { it.optionalSize } }, entity.optionalSize)
    }

    @Entity
    class TestEntity(
        val size: ByteSize,
        val optionalSize: ByteSize?,
    ) {
        @Id
        @GeneratedValue(strategy = GenerationType.UUID)
        lateinit var id: UUID
    }

    @ApplicationScoped
    class TestRepository : PanacheRepositoryBase<TestEntity, UUID>
}
