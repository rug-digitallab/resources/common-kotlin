package nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class SmallryeDurationConverterTest {
    @Inject
    private lateinit var config: DurationContainer

    @Test
    fun `Smallrye Converters should be registered automatically`() {
        Assertions.assertEquals(10.hours + 5.milliseconds, config.durationProperty)
        Assertions.assertEquals(10.seconds, config.durationFunction())
    }

    @ConfigMapping(prefix = "test")
    interface DurationContainer {
        @get:WithName("duration-property")
        @get:WithConverter(DurationLongConverter::class)
        val durationProperty: Duration

        @WithConverter(DurationLongConverter::class)
        @WithName("duration-function")
        fun durationFunction(): Duration
    }
}
