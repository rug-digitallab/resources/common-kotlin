package nl.rug.digitallab.common.kotlin.quantities.integrations.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

/**
 * This is the same test, but this one makes sure Jandex does
 * its job and exports the Singleton [QuantitiesCustomizer].
 */
@QuarkusTest
class ByteSizeDeserializerTest {
    @Inject
    private lateinit var mapper: ObjectMapper

    @ParameterizedTest
    @ValueSource(strings = ["1B", "100 MB", "100_000 KiB", "100M"])
    fun `Jackson deserializer should parse valid string values`(input: String) {
        val jacksonValue = mapper.readValue("\"$input\"", ByteSize::class.java)
        val directValue = ByteSize.fromString(input)

        Assertions.assertEquals(directValue, jacksonValue)
    }

    @ParameterizedTest
    @ValueSource(longs = [1, 100, 100_000, 100_000_000, 99_999_999])
    fun `Jackson deserializer should parse valid number values`(input: Long) {
        val jacksonValue = mapper.readValue("$input", ByteSize::class.java)
        val directValue = input.B

        Assertions.assertEquals(directValue, jacksonValue)
    }

    @ParameterizedTest
    @ValueSource(strings = ["321LI", "MB32", "MB", "432432432Q"])
    fun `Jackson deserializer should throw on invalid string values`(input: String) {
        Assertions.assertThrows(InvalidFormatException::class.java) {
            mapper.readValue("\"$input\"", ByteSize::class.java)
        }
    }

    @ParameterizedTest
    @ValueSource(doubles = [1.0, 1.1, 100.1])
    fun `Jackson deserializer should throw on invalid number values`(input: Double) {
        Assertions.assertThrows(MismatchedInputException::class.java) {
            mapper.readValue("$input", ByteSize::class.java)
        }
    }

    @ParameterizedTest
    @ValueSource(strings = ["1B", "100 MB", "100_000 KiB", "100M"])
    fun `Jackson deserializer should parse valid objects`(input: String) {
        val jacksonValue = mapper.readValue("{ \"test\": \"$input\" }", ByteSizeContainer::class.java)
        val directValue = ByteSize.fromString(input)

        Assertions.assertEquals(directValue, jacksonValue.test)
    }

    data class ByteSizeContainer(val test: ByteSize)
}
