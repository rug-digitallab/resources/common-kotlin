# Quantities Library
This library provides a `ByteSize` value class that represents a "byte size" quantity - a number of bytes. This utility class allows you to use byte sizes in your code in a user-friendly way without manually keeping track of the magnitude of your variables (MB, KB, etc). In addition, this library defines and implements the default string parsing and formatting rules for byte sizes throughout the Digital Lab tool suite.

This library also provides an Eclipse Microprofile converter for `kotlin.time.Duration`. This allows providing durations in configurations that map to configuration values with type `kotlin.time.Duration`.

## Prerequisites
A Kotlin project. Ideally, the Digital Lab Gradle plugin is used, since that will automatically include this library in your classpath. Otherwise, explicitly include this library from the appropriate repository.

## Usage

### `ByteSize`

The `ByteSize` class is implemented as a simple value class with a single `Long` as backing field. This class has a number of utility functions defined for converting the instance to a number of the correct magnitude, as well as utility functions to instantiate `ByteSize` instances from numbers. To illustrate:

```kotlin
100.MB == 100_000.KB // true
1024.B.KiB // "1" (Long)
```

In addition, the library exposes a special delegate for converting `Long` properties to `ByteSize` properties, by using the `asByteSize` delegate. This can be used as follows:

```kotlin
class MyClass(
    var rawKiloBytes: Long,
) {
    var byteValue by wrapByteSize(MyClass::rawKiloBytes, ByteSize.Magnitude.KB)
}
```

In here, any read or write to `byteValue` will be "relayed" to `rawKiloBytes` with an appropriately-converted value. The `magnitude` parameter is optional, and will default to bytes.

#### String Representations
Besides these developer utilities, the class also defines string formatting and parsing facilities that implement the standard way of representing byte sizes throughout the Digital Lab tool suite. The parser accepts only integer numbers, with quantity suffixes (`MB`, `KiB`, `G`) in SI, IEC, or Unix standards, case insensitive. Thousands separators are supported in the form of `_` only. The formatter is, in addition to the above, able to produce decimal numbers, implements `toString`, and supports `String.format`.

To aid the developer in user input parsing, this library implements a number of utility classes for integration with Jackson (for JSON/XML/CSV parsing) and Smallrye Config (for configuration management). These integrations are "hidden" in the sense that they do not pollute the classpath of users of this library, but the code will automatically be usable once the relevant dependencies are explicitly included by the user.

#### Jackson Integration
The Jackson integration facilitated by this library consists of a custom `Deserializer` implementation for `ByteSize`. Do note that in order to properly use this value class, for now, the use of [`jackson-module-kogera`](https://github.com/ProjectMapK/jackson-module-kogera) is still needed since the normal [`jackson-module-kotlin`](https://github.com/FasterXML/jackson-module-kotlin/) does not yet support value classes.

In addition to the custom deserializer (which would normally need a manual registration in the `ObjectMapper`), a custom `QuantitiesModule` is included that automatically registers the deserializer by simply calling `mapper.registerQuantitiesModule()`.

The integration is even further automated when Quarkus CDI and Quarkus Jackson are available and active: the `QuantitiesCustomizer` will handle the module registration and it is automatically injected and configured if the `ObjectMapper` instance is also injected at the use site.

Note that delegate (extension) properties are not detected by Jackson, so the `ByteSize` delegate provided in this library won't be picked up by Jackson. While it might (in theory) be possible to write a Jackson module to pick up extension properties, this is a lot of tedious and error-prone reflection work and is highly dependent on which libraries are loaded at runtime. A more practical approach for supporting `ByteSize` deserialization would be to create a subclass that includes these properties and has the right annotations to prevent conflicts.

#### Smallrye Config Integration
The Smallrye integration is facilitated by a pair of custom `Converter` implementations. One of them is automatically registered and will handle conversions to `ByteSize` - however, that one cannot be used for `@ConfigMapping` interfaces where there is a `ByteSize` property or function. This is caused by Kotlin removing the value class "box" and instructing JVM to deal with the underlying (`Long`) type directly. In addition, the JVM name will be "mangled" to prevent overloading issues. Consequently, Smallrye will see a field with a weird name of `Long` type and it does not know how to convert a `"100MB"` string to a `Long`. For such usecases, you will have to manually annotate the `ByteSize` field using `@WithConverter(ByteSizeLongConverter::class)` and explicitly specify a setting name using `@WithName("foo")`. 

#### Jakarta Integration
The Jakarta integration is intended for automatically properly converting `ByteSize` values to `Long` values for persistence in a database. The necessary converter is automatically registered, without manual intervention.

By default, Jakarta can recognize the unboxed `Long` unboxed field that is generated by Kotlin for `ByteSize` properties. Jakarta will then create a `bigint not null` column (for MySQL), and everything works immediately. For nullable properties, Kotlin will not unbox the field, so we need the custom converter to properly convert `ByteSize` boxes to the underlying `Long` representation. This is already registered and automatically picked up when needed.

### `kotlin.time.Duration`

#### Smallrye Config Integration
The Smallrye integrate is facilitated in exactly the same manner as for `ByteSize`, except we replace `ByteSize` with `Duration`, since `Duration` is also a value class represented by a `Long`.
