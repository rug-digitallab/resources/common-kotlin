rootProject.name = "common-kotlin"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.kotlin.library") version digitalLabGradlePluginVersion
        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Maven
        gradlePluginPortal()
    }
}

include("approval-tests")
include("helpers")
include("quantities")
include("quantities:test")
