plugins {
    id("nl.rug.digitallab.gradle.plugin.kotlin.library")
}

dependencies {
    val approvalTestsVersion: String by project

    implementation(project(":helpers"))
    api("com.approvaltests:approvaltests:$approvalTestsVersion")
}

tasks.test {
    useJUnitPlatform()
}
