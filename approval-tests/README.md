# Approval Tests Library
This library standardizes the way to use Approval Tests in the Digital Lab codebase. 

## Prerequisites
A Kotlin project that includes this library.

```kotlin
testImplementation("nl.rug.digitallab.common.kotlin:approval-tests:x.y.z")
```

## Usage

Using the Approval Tests library is simple in combination with the `ApprovalTestSpec` helper class. First, create the 
`ApprovalTestSpec`:

```kotlin
val approvalTestSpec = ApprovalTestSpec(
    outputDirectory = "tests/my/output/dir",
    name = "my-test-name",
    attributes = listOf("my-test-tags", "tag2")
)
```

And then, call `Approvals.verify` with the `approvalTestSpec`:

```kotlin
Approvals.verify("my expected output", approvalTestSpec.toOptions())
```

It is also possible to generate the Approval Test output next to the input file. This approach requires the 
`.approvaltests-anchor` file to be created in the `resources` directory. 

```kotlin
val inputPath = getResource("tests/my/test/with.input").toURI().toPath()
val expectedOutput = Files.readString(inputPath)

val approvalTestSpec = ApprovalTestSpec.from(
    inputPath,
    *arrayOf("spec"),
)

Approvals.verify(expectedOutput, approvalTestSpec.toOptions())
```

Finally, add a `.gitattributes` file to your project root with the following content:

```
# We want to disable automatic merging of approval test outputs, since
# the outputs are not guaranteed to be stable. This might cause issues
# where after a merge, tests suddenly and unpredictably fail. By disabling
# merging, we force a merge conflict and require the tests to be rerun
# (and the output to be regenerated).
# In addition, we mark the file to be auto-generated, so any diffs will
# be hidden by default in the GitLab MR UI, but they can be shown on request.
*.approved.* -merge text gitlab-generated
```

For complete examples, see the [ApprovalTestSpecTest.kt](src/test/kotlin/nl/rug/digitallab/common/kotlin/approvaltests/ApprovalTestSpecTest.kt) file.
