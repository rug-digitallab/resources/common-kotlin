package nl.rug.digitallab.common.kotlin.approvaltests

/**
 * These specify some settings for our Approval Testing setup. This
 * is not a very conventional setup, but the Approval Tests library
 * looks for this class through reflection.
 */
class PackageSettings {
    // This needs to be a class with a companion object, instead of a
    // top-level object, since the reflection that is done by the
    // Approval Tests library will try to instantiate the `PackageSettings`
    // class. This doesn't work for top-level objects, as those are not
    // instantiable in JVM.
    companion object {
        // Instead of storing our test outputs near the testing code,
        // for the usecases of this test suite we put the outputs
        // near the inputs that are in resources.
        const val ApprovalBaseDirectory: String = "../resources"
    }
}
