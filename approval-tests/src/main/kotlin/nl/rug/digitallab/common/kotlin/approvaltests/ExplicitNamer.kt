package nl.rug.digitallab.common.kotlin.approvaltests

import com.spun.util.tests.StackTraceReflectionResult
import com.spun.util.tests.TestUtils
import org.approvaltests.core.Options
import org.approvaltests.namer.ApprovalNamer
import org.approvaltests.namer.AttributeStackSelector
import org.approvaltests.namer.NamerFactory
import org.approvaltests.namer.StackTraceNamer
import java.io.File

/**
 * Custom "Namer" implementation to give more control over the Approval Test
 * output files. This extends the normal [StackTraceNamer], as that already
 * implements the lookup logic for finding the source files.
 *
 * @property directory The directory in which to store the test outputs, relative
 *                     to the `ApprovalBaseDirectory` configurable through `PackageSettings`
 * @property testName The base name of the output file, which should correspond to
 *                    the name of the current test.
 * @property additionalInformation Any additional information as used elsewhere in the
 *                                 ApprovalTests library - such as platform, suffixes, etc.
 */
class ExplicitNamer: StackTraceNamer {
    private val directory: String
    private val testName: String
    private val additionalInformation: String

    private constructor(
        directory: String,
        testName: String,
        info: StackTraceReflectionResult,
        additionalInformation: String,
    ): super(info, additionalInformation) {
        this.directory = directory
        this.testName = testName
        this.additionalInformation = additionalInformation
    }

    constructor(
        directory: String,
        testName: String,
    ) : this(
        directory = directory,
        testName = testName,
        info = TestUtils.getCurrentFileForMethod(AttributeStackSelector()),
        additionalInformation = NamerFactory.getAndClearAdditionalInformation()
    )

    override fun getApprovalName() = testName + additionalInformation

    override fun getBaseDirectory(): String {
        // The logic in this function is identical to the base logic, except for the part
        // where we use the `directory` instead of the package path to find the output location.
        var baseDir = info.sourceFile.absolutePath
        if(NamerFactory.getApprovalBaseDirectory().isNotBlank()) {
            val packageName = info.fullClassName.substring(0, info.fullClassName.lastIndexOf("."))
            val packagePath = File.separator + packageName.replace('.', File.separatorChar)
            val currentBase = baseDir.substring(0, baseDir.indexOf(packagePath) + 1)
            val newBase = currentBase + NamerFactory.getApprovalBaseDirectory() + File.separator + directory
            baseDir = File(newBase).canonicalPath.toString()
        }
        return baseDir
    }

    override fun addAdditionalInformation(newInformation: String?): ApprovalNamer {
        return ExplicitNamer(directory, testName, info, "$additionalInformation.$newInformation")
    }

    companion object {
        /**
         * Helper function to use the [ExplicitNamer] naming strategy, for the given [directory] and [testName]
         *
         * @param directory The directory in which to store the test outputs, relative
         *                  to the `ApprovalBaseDirectory` configurable through `PackageSettings`
         * @param testName The base name of the output file, which should correspond to
         *                 the name of the current test.
         */
        fun Options.forExplicitFile(directory: String, testName: String): Options {
            return this.forFile().withNamer(ExplicitNamer(directory, testName))
        }

        /**
         * Helper function to easily add additional parameters to a given naming strategy. Similar
         * to [org.approvaltests.Approvals.NAMES.withParameters].
         */
        fun Options.withParameters(vararg parameters: String): Options {
            return parameters.fold(this) { option, parameter -> option.forFile().withAdditionalInformation(parameter) }
        }
    }
}

