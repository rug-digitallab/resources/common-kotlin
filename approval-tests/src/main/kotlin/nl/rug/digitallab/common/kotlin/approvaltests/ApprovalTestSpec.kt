package nl.rug.digitallab.common.kotlin.approvaltests

import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.kotlin.approvaltests.ExplicitNamer.Companion.forExplicitFile
import nl.rug.digitallab.common.kotlin.approvaltests.ExplicitNamer.Companion.withParameters
import org.approvaltests.core.Options
import java.nio.file.Path
import kotlin.io.path.*

/**
 * Helper class to specify an approval test. This provides a more user-friendly
 * wrapper to construct an Approval Options object. In particular, this assumes
 * the convention in [PackageSettings] that all test outputs will be in the
 * resources directory, instead of the code sources.
 *
 * ```kotlin
 * val spec = ApprovalTestSpec(
 *     outputDirectory = "foo/bar",
 *     name = "testName",
 *     attributes = listOf("here", "we", "go")
 * )
 * Approvals.verify(..., spec.toOptions())
 *
 * // Will create a file "./resources/foo/bar/testName.here.we.go.approved.txt"
 * ```
 *
 * @param outputDirectory The directory name where the test outputs will be stored.
 *                        This should be relative to the "resources" directory
 * @param name The test name, used as the first component in the approval text file.
 *             This name typically corresponds to an input file name, to clearly link
 *             the input and outputs together.
 * @param attributes A list of further attributes ("parameters" in Approval Tests
 *                   lingo) to be put in the output file name. These represent various
 *                   "aspects" of a single test definition.
 */
data class ApprovalTestSpec(
    val outputDirectory: String,
    val name: String,
    val attributes: List<String>,
) {
    /**
     * Secondary constructor for [ApprovalTestSpec] to simplify the specification
     * of the [outputDirectory], as this will automatically deduce the path relative
     * to the resources directory. This requires a special file "approvalsAnchor"
     * to be present in the root of the resources directory.
     *
     * In addition, this supports a vararg-style specification of attributes. This is
     * not possible on the primary constructor for a data class.
     */
    constructor(
        outputDirectory: Path,
        name: String,
        vararg attributes: String,
    ): this(
        outputDirectory.relativeTo(getResource(".approvaltests-anchor").toURI().toPath().parent).pathString,
        name,
        attributes.toList(),
    )

    fun toOptions(): Options =
        Options()
            .forExplicitFile(outputDirectory, name)
            .withParameters(*attributes.toTypedArray())

    companion object {
        /**
         * Create an [ApprovalTestSpec] based on the given test input file. The test
         * outputs will be stored "next to" the input file - in the same directory,
         * and with the same base name.
         *
         * For example, for a file "./tests/foo-bar.input", this will cause output
         * files to be stored as "./tests/foo-bar.<attributes>.approved.txt"
         *
         * @param inputPath The path to the test input file
         * @param attributes Additional attributes to be put in the output file name.
         *                   These typically represent various "aspects" of a single
         *                   test definition.
         *
         * @return The instantiated [ApprovalTestSpec]
         */
        fun from(
            inputPath: Path,
            vararg attributes: String,
        ) = ApprovalTestSpec(
            outputDirectory = inputPath.parent,
            name = inputPath.nameWithoutExtension,
            attributes = attributes
        )
    }
}

