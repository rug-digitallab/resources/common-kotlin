package nl.rug.digitallab.common.kotlin.approvaltests

import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import org.approvaltests.Approvals
import org.junit.jupiter.api.Test
import java.nio.file.Files
import kotlin.io.path.toPath

class ApprovalTestSpecTest {
    @Test
    fun `An approval test that uses generated input should succeed`() {
        // input that is generated during the test
        val expectedContents = "An approval test that uses generated input should succeed"

        // The approval test output is stored in the specified directory
        val approvalTestSpec = ApprovalTestSpec(
            outputDirectory = "tests/spec/test1",
            name = "test1",
            attributes = listOf("spec")
        )

        Approvals.verify(expectedContents, approvalTestSpec.toOptions())
    }

    @Test
    fun `An approval test that uses input read from the file system should succeed`() {
        val inputPath = getResource("tests/spec/test2/test2.input").toURI().toPath()

        // input that is read from the file system
        val expectedContents = Files.readString(inputPath)

        // The approval test output is stored next to the input file
        // Depends on the .approvaltests-anchor to resolve the paths.
        val approvalTestSpec = ApprovalTestSpec.from(
            inputPath,
            *arrayOf("spec"),
        )

        Approvals.verify(expectedContents, approvalTestSpec.toOptions())
    }
}
